# IQRF Gateway OS Dev

IQRF Gateway OS development environment for the RPI4-based target.

# Download the image

```bash
wget https://dl.iqrf.org/iqrf-gateway-os-dev/iqrf-gatewayosdev-image-raspberrypi4-20211105.rootfs.wic.bmap
wget https://dl.iqrf.org/iqrf-gateway-os-dev/iqrf-gatewayosdev-image-raspberrypi4-20211105.rootfs.wic.bz2
```

# Flash the image to 8GB SD card

```bash
sudo apt install bmap-tools
sudo bmaptool copy --bmap iqrf-gatewayosdev-image-raspberrypi4-20211105.rootfs.wic.bmap iqrf-gatewayosdev-image-raspberrypi4-20211105.rootfs.wic.bz2 /dev/sda
```

# SSH login 

```bash
sudo systemctl enable ssh
sudo systemctl start ssh
```

- credentials: admin/admin
