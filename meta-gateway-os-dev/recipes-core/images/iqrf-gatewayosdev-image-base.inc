# Base this image on core-image-base
include recipes-core/images/core-image-base.bb

# - debug-tweaks
#   - empty-root-password
#   - allow-empty-password
#   - allow-root-login
#   - post-install-logging

IMAGE_FEATURES = "ssh-server-openssh \
		  dev-pkgs \
		  dbg-pkgs \
		  tools-sdk \
		  tools-debug \
		  tools-profile \
		  ptest-pkgs \
		  debug-tweaks \
		  bash-completion-pkgs \
"

IMAGE_INSTALL = "packagegroup-core-boot \
		packagegroup-base \
		${CORE_IMAGE_EXTRA_INSTALL} \
		kernel-modules \
		wpa-supplicant \
		shape shapeware \
		networkmanager \
		monit \
		nano \
		sudo \
		mosquitto \
		mosquitto-clients \
		avahi-daemon \
		rsyslog \
		networkmanager \
		modemmanager \
		wireguard-tools \
		wireguard-client-config \
		usb-modeswitch \
		coreutils \
		logrotate \
		watchdog \
		watchdog-config \
		pixla-client \
		jq \
		htop \
		bash \
		bash-completion \
		haveged \
		rng-tools \
		iotop \
		fake-hwclock \
		tzdata \
		tzdata-europe \
		wget rsync curl \
		screen \
		systemd-conf \
		base-config \
		openssh-sshd \
		linux-firmware-rtl8188 \
		googletest \
		i2c-tools \
		cmake \
		git \
"
GLIBC_GENERATE_LOCALES = "en_GB.UTF-8 en_US.UTF-8"
IMAGE_LINGUAS ?= "en-gb"

inherit core-image extrausers

# set admin password for all images
EXTRA_USERS_PARAMS = "useradd -P admin admin; \
	usermod -a -G root,sudo admin; \
	groupadd admin; "
