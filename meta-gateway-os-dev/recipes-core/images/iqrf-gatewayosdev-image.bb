require iqrf-gatewayosdev-image-base.inc

# necessary iqrf components for base image
IMAGE_INSTALL += " \
		iqrf-gateway-controller \
		iqrf-gateway-daemon \
		iqrf-gateway-setter \
		iqrf-gateway-webapp \
		iqrf-gateway-uploader \
		iqrf-scripts \
"

# necessary components for the apps image
IMAGE_INSTALL += " \
		nodejs-npm \
		node-red \
		python3-pip \
"
