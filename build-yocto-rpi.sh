#!/bin/sh

if [ ! -z "$1" ]; then
	kas shell iqrf-gateway-os-dev-rpi.yml -c "$@" 
else
	kas build iqrf-gateway-os-dev-rpi.yml
fi
